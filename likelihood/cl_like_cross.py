import numpy as np
import cPickle as pickle
import pdb
from cosmosis.datablock import option_section, names as section_names
import os

DIRNAME=os.path.split(__file__)[0]
NBIN=20
SPLIT_BIN=10
NELL=50

class LikelihoodCalculator(object):
	def __init__(self,dirname=DIRNAME,save=False):
		covmat_file=os.path.join(dirname,'covmat.txt')
		data_file=os.path.join(dirname,'data.txt')
		ell_file=os.path.join(dirname,'ell.txt')
		self.ell    = np.loadtxt(ell_file)
		self.covmat = self.load_covmat(covmat_file,NBIN,NELL,save=save)
		self.mu     = self.load_measurement(data_file,NBIN,NELL,save=save)
		self.weight = np.linalg.inv(self.covmat)
		#pdb.set_trace()
	def load_measurement(self,filename,nbin,nell,save=False):
		bins1 = list(range(1,SPLIT_BIN+1))
		bins2 = list(range(SPLIT_BIN,nbin+1))
		pair=0
		mu_data={}
		for line in open(filename):
			line=line.strip()
			if not line or line.startswith('#'): continue
			words=line.split()
			i=int(words[0])
			j=int(words[1])
			m = np.array([float(x) for x in words[2:]])
			assert len(m)==nell, "Data did not have the right number of ell"
			mu_data[(i,j)]=m
		mu = []
		if save: f=open("mycl.txt","w")
		for i in xrange(1,nbin+1):
			for j in xrange(1,nbin+1):
				if j>i:continue
				if i in bins1 and j in bins1: continue
				if i in bins2 and j in bins2: continue
				m=mu_data[(i,j)]
				if save:
					for ell in xrange(nell):
						f.write("%d  %d  %d  %e\n"%(i,j,self.ell[ell],m[ell]))
				mu.append(m)
		if save: f.close()
		return np.concatenate(mu)



	def load_covmat(self,filename,nbin,nell,save=False):
		#Load file into dictionary by bin
		bins1 = list(range(1,SPLIT_BIN+1))
		bins2 = list(range(SPLIT_BIN,nbin+1))
		#npair=nbin*(nbin+1)/2
		cross_pairs=[(i,j) for i in bins1 for j in bins2]
		npair=len(cross_pairs)
		ndata=npair*nell
		ndata=4500
		covmat_data={}
		for line in open(filename):
			line=line.strip()
			if not line or line.startswith('#'): continue
			words=line.split()
			i=int(words[0])
			j=int(words[1])
			m=int(words[2])
			n=int(words[3])
			C = np.array([float(x) for x in words[4:]])
			covmat_data[(i,j,m,n)] = C
			# import pylab
			# print i,j,m,n
			# pylab.semilogy(C)
			# pylab.show()
			# sys.exit(0)

		#pdb.set_trace()
		#Turn dictionary into matrix
		M=np.zeros((ndata,ndata))
		pair1=0
		if save: f=open("mycov.txt","w")
		for i in xrange(1,nbin+1):
			for j in xrange(1,nbin+1):
				if j>i:continue
				if i in bins1 and j in bins1: continue
				if i in bins2 and j in bins2: continue
				pair2=0
				for m in xrange(1,nbin+1):
					for n in xrange(1,nbin+1):
						if n>m:continue
						if n in bins1 and m in bins1: continue
						if n in bins2 and m in bins2: continue
						c=covmat_data[(i,j,m,n)]
						ell0_i=nell*pair1
						ell0_j=nell*pair2
						for ell in xrange(nell):
							M[ell0_i+ell,ell0_j+ell]=c[ell]
							#print ell0_i+ell,ell0_j+ell
							if save: f.write("%d  %d  %d   %d   %d  %e\n"%(i,j,m,n,ell,c[ell]))
						pair2+=1
				pair1+=1
		if save: f.close()
		#pdb.set_trace()
		#pickle.dump(M, open('output_covmat.p', 'wb'))
		return M

	def interpolate_to_data_ell(self, ell_theory, cl_theory_bins):
		output={}
		ell_data = self.ell
		for ij,cl in cl_theory_bins.items():
			output[ij] = np.interp(ell_data, ell_theory, cl)
		return output



	def likelihood(self, nbin, split_bin, ell, cl_bins):
		bins1 = list(range(1,split_bin+1))
		bins2 = list(range(split_bin,nbin+1))
		cl_bins = self.interpolate_to_data_ell(ell, cl_bins)
		chi2=0.0
		c=[]
		for i in xrange(1,nbin+1):
			for j in xrange(1,nbin+1):
				if j>i:continue
				if i in bins1 and j in bins1: continue
				if i in bins2 and j in bins2: continue
				c_ij=cl_bins[i,j]
				c.append(c_ij)
		d=np.concatenate(c)-self.mu
		savdata = {
					 'dvec' : d,
					 'muvec' : self.mu,
					 #'covmat' : self.covmat,
					 'cvec' : c,
					 'weight' : self.weight
						}
		pickle.dump(savdata, open('auto_data.p', 'wb'))
		chi2=np.dot(d,np.dot(self.weight,d))
		return -0.5*chi2, cl_bins

def setup(options):
	dirname=options[option_section, 'dirname']
	split_bin = options[option_section, 'experiment_split_bin']
	print "Simulated shear spectra in %s" % dirname
	return LikelihoodCalculator(dirname)


def execute(block,config):
	calculator=config

	cl_data = {}
	section = section_names.shear_cl
	nbin = block[section,"NBIN"]
	ell = block[section,"ELL"]
	bins1 = list(range(1,SPLIT_BIN+1))
	bins2 = list(range(SPLIT_BIN,nbin+1))

	#pdb.set_trace()
	assert nbin==NBIN, "Wrong number of bins generated in data."
	for i in xrange(1,nbin+1):
		for j in xrange(1,nbin+1):
			if j>i:continue
			if i in bins1 and j in bins1: continue
			if i in bins2 and j in bins2: continue
			column='BIN_%d_%d' % (i,j)
			cl_data[(i,j)] = block[section, column]
	like, interpolated_cl = calculator.likelihood(nbin,SPLIT_BIN,ell,cl_data)
	section=section_names.likelihoods
	block[section, "WL_LIKE"] = like

	section="INTERP_SHEAR_CL"
	block[section, "NBIN"] = nbin
	for (i,j), spectrum in interpolated_cl.items():
		column='BIN%d_%d' % (i,j)
		block[section, column] =  spectrum

	return 0 